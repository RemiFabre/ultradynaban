import pypot.dynamixel
import time
import math

ports = pypot.dynamixel.get_available_ports()
dxl_io = pypot.dynamixel.DxlIO(ports[1], baudrate=1000000)
dxl_io.get_present_position([1])
dxl_io.enable_torque({1:1})

t0 = time.time()
previousT = t0

while True :
    try :
        t = time.time() - t0
        f = 0.25
        angle = 58 + 30*math.sin(2*math.pi*f*t)
        dxl_io.set_goal_position({1:int(angle)})
        #dxl_io.set_goal_position({1:int(15)})
        #dxl_io.get_present_position([1])[0]
        #print (str(dxl_io.get_present_position([1])[0]) + " " + str(angle))

        freq = 1/float(t - previousT)
        previousT = t
        print("{} Hz".format(freq))
        time.sleep(0.01)
        
    except Exception as e:
        print("Exception : ", e)
