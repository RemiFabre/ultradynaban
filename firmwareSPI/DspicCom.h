/*
 * DspicCom.h
 *
 */

#ifndef DSPIC_COM_H
#define DSPIC_COM_H

#include "CommonForDspic.h"
#include "libmaple/libmaple_types.h"

class DspicCom
{
public:
	explicit DspicCom(uint8 pin_SS);
	DspicCom(uint8 pin_SS, uint32 spi_number);
	~DspicCom();

	void Init();
	void SetDefaultOrder(uint16 order);
	void Refresh();
	void Refresh(uint16 order);

	void SetPwmBridge(int16 pwm);
	void SetPwmFan1(uint16 pwm);
	void SetPwmFan2(uint16 pwm);
	void SetConfig(uint16 config);

	uint16 GetTorque();
	uint16 GetInternalCodeur();
	uint16 GetMagneticCodeur();
	uint16 GetBridgeCurrent();
	int16 GetBridgeVoltage();
	uint16 GetBridgeTemp();
	uint16 GetMotorTemp();
	uint16 GetBatteryTemp();
	uint16 GetBatteryVoltage();
	uint16 GetErrors();


private:
	void RefreshData(uint16 order);
	void SendData(uint16 data);
	void SendSignedData(int16 data);
	uint16 ReceiveData();
	HardwareSPI spiMaple;
	uint8 m_spi_SS_pin;
	uint16 m_default_order;

	int16 m_pwmBridge;
	uint16 m_torque;
	uint16 m_qei;
	uint16 m_codeurMagn;
	uint16 m_currentBridge;
	int16 m_vout;
	uint16 m_tempBridge;
	uint16 m_tempMotor;
	uint16 m_tempBatt;
	uint16 m_vBatt;
	uint16 m_pwmFan1;
	uint16 m_pwmFan2;
	uint16 m_error;
	uint16 m_config;
};


#endif //DSPIC_COM_H
