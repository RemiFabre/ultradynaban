/*
 * CommonForDspic.h
 *
 */

#ifndef COMMON_FOR_DSPIC_H
#define COMMON_FOR_DSPIC_H

#ifndef CONSTANTES
#define CONSTANTES

#define DELAY_AFTER_ORDER 8
#define DELAY_BETWEEN_COMMAND 5
#define DELAY_AFTER_COMM 2

#define MAX_ANGLE 4095

#define DSPIC_STOP_ENABLE 1
#define DSPIC_STOP_DISABLE 0
#define DSPIC_LAUNCH_ENABLE 1
#define DSPIC_LAUNCH_DISABLE 0
#define RECEIVE_TORQUE_ENABLE 1
#define RECEIVE_TORQUE_DISABLE 0
#define SEND_PWM_BRIDGE_ENABLE 1
#define SEND_PWM_BRIDGE_DISABLE 0
#define RECEIVE_QEI_ENABLE 1
#define RECEIVE_QEI_DISABLE 0
#define RECEIVE_QEI_ENABLE 1
#define RECEIVE_QEI_DISABLE 0
#define RECEIVE_ABSOLUE_ENABLE 1
#define RECEIVE_ABSOLUE_DISABLE 0
#define RECEIVE_CURRENT_ENABLE 1
#define RECEIVE_CURRENT_DISABLE 0
#define RECEIVE_VOUT_ENABLE 1
#define RECEIVE_VOUT_DISABLE 0
#define RECEIVE_TEMP_BRIDGE_ENABLE 1
#define RECEIVE_TEMP_BRIDGE_DISABLE 0
#define RECEIVE_TEMP_MOTOR_ENABLE 1
#define RECEIVE_TEMP_MOTOR_DISABLE 0
#define RECEIVE_TEMP_BATT_ENABLE 1
#define RECEIVE_TEMP_BATT_DISABLE 0
#define RECEIVE_V_BATT_ENABLE 1
#define RECEIVE_V_BATT_DISABLE 0
#define SEND_PWM_FAN1_ENABLE 1
#define SEND_PWM_FAN1_DISABLE 0
#define SEND_PWM_FAN2_ENABLE 1
#define SEND_PWM_FAN2_DISABLE 0
#define RECEIVE_ERROR_ENABLE 1
#define RECEIVE_ERROR_DISABLE 0
#define SEND_CONF_ENABLE 1
#define SEND_CONF_DISABLE 0


#define SHT_STOP            15
#define SHT_ENABLE          14
#define SHT_Torque          13
#define SHT_PWM_Bridge      12
#define SHT_QEI             11
#define SHT_Absolu          10
#define SHT_Curent_Bridge   9
#define SHT_Vout            8
#define SHT_Temp_Bridge     7
#define SHT_Temp_Motor      6
#define SHT_Temp_Bat        5
#define SHT_V_Bat           4
#define SHT_PWM_Fan1        3
#define SHT_PWM_Fan2        2
#define SHT_Error           1
#define SHT_Config          0

#endif //CONSTANTES

#include <wirish/wirish.h>
#include <libmaple/libmaple_types.h>

uint16 GetOrder(int stopState,
				int enableState,
				int torqueState,
				int pwmBridgeState,
				int qeiState,
				int absoluState,
				int currentState,
				int voutState,
				int tempBridgeState,
				int tempMotor,
				int tempBatt,
				int vBattState,
				int pwmFan1State,
				int pwmFan2State,
				int errorState,
				int configState);

#endif //COMMON_FOR_DSPIC_H
