#include <wirish/wirish.h>
#include <libmaple/adc.h>
#include <libmaple/timer.h>
#include "dxl_HAL.h"
#include "motor.h"
#include "control.h"
#include "dxl.h"
#include "trajectory_manager.h"
#include "flash_write.h"
#include "DspicCom.h"

#define SS_DSPIC PA4   //ss
#define RESET_DSPIC PC15 //D12

//#define ORDER_SHORT 0x2E00//torque, pos1,pos2,current
//#define ORDER_LONG 0x2FF0//every sensors

/**
 * Schedules the hardware tasks
 */
void hardware_tick();
void set_ready_to_update_hardware();
void reset_pic();
void refresh_hardware_struct();
int my_atoi(char *p, bool *success);
void read_hardware(bool is_first_read = false);
void debug();

long counter = 0;
unsigned int hardwareCounter = 0;
bool readyToUpdateHardware = false;
int index = 0;
HardwareTimer timer2(2);
DspicCom dspic(SS_DSPIC, 1);
encoder *encoder0;
unsigned char controlMode = OFF;
hardware hardwareStruct;
char led_value = 0;
bool led_on = false;
uint32 time_ms = 0;
bool stop = false;
uint16 order_big;
// SPI1 PA7=MOSI, PA6=MISO, PA5=SCK, PA4=NSS
// Pins on the maple mini: PA7=4, PA6=5, PA5=6, PA4=7

//UI
void print_help();
int UI_printf_debug = 0;
int UI_reset_dspic = 0;


void setup()
{
	disableDebugPorts();

	pinMode(BOARD_LED_PIN, OUTPUT);
	pinMode(RESET_DSPIC, OUTPUT);
	SerialUSB.begin();
	Serial1.begin(115200);

	timer2.pause();
	// The hardware will be read at 1Khz
	timer2.setPrescaleFactor(72);
	timer2.setOverflow(1000);
	timer2.setChannel1Mode(TIMER_OUTPUT_COMPARE);
	// Interrupt 1 count after each update
	timer2.setCompare(TIMER_CH1, 1);
	timer2.attachCompare1Interrupt(set_ready_to_update_hardware);
	timer2.refresh();
	timer2.resume();

	delay(1);

	//Init the SPI module selected of the Maple Mini Board.
	dspic.Init();
	pinMode(SS_DSPIC, OUTPUT);
	delay(10);
	//dspic.Refresh(0x4000); //démarrage du pic

	//Setup your orders. For that, you can use "GetOrder" function from CommonForDspic...
	order_big = GetOrder(DSPIC_STOP_DISABLE,
						 DSPIC_LAUNCH_ENABLE,
						 RECEIVE_TORQUE_DISABLE,
						 SEND_PWM_BRIDGE_ENABLE,
						 RECEIVE_QEI_ENABLE,
						 RECEIVE_ABSOLUE_ENABLE,
						 RECEIVE_CURRENT_ENABLE,
						 RECEIVE_VOUT_ENABLE,
						 RECEIVE_TEMP_BRIDGE_ENABLE,
						 RECEIVE_TEMP_MOTOR_ENABLE,
						 RECEIVE_TEMP_BATT_ENABLE,
						 RECEIVE_V_BATT_ENABLE,
						 SEND_PWM_FAN1_DISABLE,
						 SEND_PWM_FAN2_DISABLE,
						 RECEIVE_ERROR_ENABLE,
						 SEND_CONF_DISABLE);

	//You can set a default order for the communication with the dspic.
	dspic.SetDefaultOrder(order_big);

	// The firs read calls motor_init() with the read values
	read_hardware(true);

	//Traj
	//predictive_control_init();

	//Control
	control_init();

	hardwareStruct.mot = motor_get_motor();

	delay(2000);
}

int nombre = 0;
void loop()
{
	if (readyToUpdateHardware) {
		counter++;
		readyToUpdateHardware = false;
		hardware_tick();
	}

	if (SerialUSB.available()) {
		char c = SerialUSB.read();

		if ((c >= '0') && (c <= '9')) {
			nombre = ((nombre * 10) + (c - '0'));
		}

		if ((c >= 'A') && (c <= 'Z')) {

			if (c == 'H') {//HELP
				print_help();
			}

			if (c == 'D') {//DEBUG : actif ou non
				UI_printf_debug = ~UI_printf_debug;
			}

			if (c == 'R') {//RESET : reset dspic
				UI_reset_dspic = 1;
			}

			if (c == 'A') {//ANGLE : entrer manuellement un angle
				SerialUSB.print("new angle : ");
				SerialUSB.println(nombre);
				motor_set_target_angle(nombre);
			}
			nombre = 0;
		}
	}
}

void hardware_tick()
{
	if (stop) {
		return;
	}

	if (UI_reset_dspic) {
		reset_pic();
		UI_reset_dspic = 0;
	}

	//These actions are performed at a rate of 1kHz
	hardwareCounter++;
	time_ms = time_ms + 1;
	read_hardware();
	motor_update(hardwareStruct.codeurMagn, hardwareStruct.qei,
				 hardwareStruct.currentBridge);
	//motor_set_target_angle(3472 + 100*sin(2*3.1415*1*time_ms/1000));

	int16 deltaAngle = control_angle_diff(hardwareStruct.mot->targetAngle,
										  hardwareStruct.mot->angle);

	motor_set_command(deltaAngle * 32);
	dspic.SetPwmBridge(hardwareStruct.mot->command);

	//int16 pwm = 2047*sin(2*3.1415*0.1*time_ms/1000);
	//int16 pwm = ((time_ms/5000)%6)*-400;
	//set_pwm(pwm);
	if (hardwareCounter > 500) {

		if (UI_printf_debug)
			debug();

		//These actions are performed at ~2 Hz
		led_on = !led_on;
		if (led_on) {
			digitalWrite(BOARD_LED_PIN, LOW);
		} else {
			digitalWrite(BOARD_LED_PIN, HIGH);
		}

		hardwareCounter = 0;
	}
}

void set_ready_to_update_hardware()
{
	readyToUpdateHardware = true;
}

// Force init to be called *first*, i.e. before static object allocation.
// Otherwise, statically allocated objects that need libmaple may fail.
__attribute__((constructor)) void premain()
{
	init();
}

int main(void)
{
	setup();
	reset_pic();
	dspic.Refresh(0x4000); //démarrage du pic
	motor_set_target_angle(3472);
	while (true) {
		loop();
	}

	return 0;
}


int my_atoi(char *p, bool *success)
{
	int k = 0;
	int sign = 1;
	if (*p == '-') {
		sign = -1;
		p++;
	}
	while (*p != '\0') {
		int value = *p - '0';
		if (value >= 0 && value <= 9) {
			k = k * 10 + value;
			p++;
		} else {
			*success = false;
			return 0;
		}
	}
	*success = true;
	return k * sign;
}

void read_hardware(bool is_first_read)
{
	uint16 inc_pos = 0;
	uint16 abs_pos = 0;
	uint16 current = 0;

	//dspic.Refresh(0x2E00);
	//uint16 order = (1U<<SHT_QEI) | (1U<<SHT_Absolu) | (1U<<SHT_Curent_Bridge);
	//SerialUSB.println("test");
	dspic.Refresh();
	refresh_hardware_struct();

	inc_pos = dspic.GetInternalCodeur();
	abs_pos = dspic.GetMagneticCodeur();
	current = dspic.GetBridgeCurrent();


	// Attention ! Putting an uint16 into an int16. Works cos the range is [0, 4095]
	// To be fixed
	if (is_first_read) {
		//SerialUSB.println("init");
		motor_init(abs_pos, inc_pos);
	} else {
		//SerialUSB.println("update");
		motor_update(abs_pos, inc_pos, current);
	}

}

void debug()
{
	int16 deltaAngle = control_angle_diff(hardwareStruct.mot->targetAngle,
										  hardwareStruct.mot->angle);

	SerialUSB.println("==============================");

	SerialUSB.print("position = ");
	SerialUSB.print(hardwareStruct.mot->angle);
	SerialUSB.print(", inc = ");
	SerialUSB.print(hardwareStruct.mot->inc_angle);
	SerialUSB.print(", abs = ");
	SerialUSB.print(hardwareStruct.codeurMagn);
	SerialUSB.print(", current = ");
	SerialUSB.print(hardwareStruct.mot->current);
	SerialUSB.print(", error = ");
	SerialUSB.println(hardwareStruct.error);

	SerialUSB.print("vBatt = ");
	SerialUSB.print(hardwareStruct.vBatt);
	SerialUSB.print(", vOut = ");
	SerialUSB.print(dspic.GetBridgeVoltage());
	SerialUSB.print(", tempBridge = ");
	SerialUSB.print(hardwareStruct.tempBridge);
	SerialUSB.print(", tempMotor = ");
	SerialUSB.print(dspic.GetMotorTemp());
	SerialUSB.print(", tempBatt = ");
	SerialUSB.println(hardwareStruct.tempBatt);
	SerialUSB.println();


	SerialUSB.print("PWM = ");
	SerialUSB.println(hardwareStruct.mot->command);
	SerialUSB.print("targetAngle = ");
	SerialUSB.println(hardwareStruct.mot->targetAngle);
	SerialUSB.print("deltaAngle = ");
	SerialUSB.println(deltaAngle);

}

void reset_pic()
{
	digitalWrite(RESET_DSPIC, LOW); // Resetting the dspic
	delay(50);
	digitalWrite(RESET_DSPIC, HIGH);
	delay(150);
}

void refresh_hardware_struct()
{
	hardwareStruct.torque = dspic.GetTorque();
	hardwareStruct.qei = dspic.GetInternalCodeur();
	hardwareStruct.codeurMagn = dspic.GetMagneticCodeur();
	hardwareStruct.currentBridge = dspic.GetBridgeCurrent();
	hardwareStruct.vout = dspic.GetBridgeVoltage();
	hardwareStruct.tempBridge = dspic.GetBridgeTemp();
	hardwareStruct.tempMotor = dspic.GetMotorTemp();
	hardwareStruct.tempBatt = dspic.GetBatteryTemp();
	hardwareStruct.vBatt = dspic.GetBatteryVoltage();
	hardwareStruct.error = dspic.GetErrors();
}

void print_help()
{
	SerialUSB.println("==============================");
	SerialUSB.println("HEEEEELP\n");
	SerialUSB.println("  H : Launch this help prompt");
	SerialUSB.println("  D : Enable/Disable debug");
	SerialUSB.println("  R : Reset dsPIC on the power board");
	SerialUSB.println("  A : Enter manually a target angle");
	SerialUSB.println("    > Enter the number, then type A");
}

