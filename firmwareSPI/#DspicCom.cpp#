//
// Created by CopperBot on 10/05/2017.
//

#include <SPIcustom.h>
#include "DspicCom.h"
#include "CommonForDspic.h"

//On force l'utilisation de du SPIcustom, dans l'éventualité de modifier le spi de la maple.
SPIClassCustom spiMapleCustom(1);

/**
 * initSPI()
 * Fonciton d'initialisation du SPI de la mapleMini.
 * On travaille à 9MHZ pour le moment, la communication "plante" au dela.
 * (optimisation à faire dans le SPIcustom (dma ?) pour éventuellement regler le problème et gagner en vitesse)
 * Le mode choisi doit concorder avec celui mis en place sur le dsPIC.
 * Un delay suplémentaire est ajouté par sécurité (à revoir)
 */
void DspicCom::initSPI(){

    digitalWrite(SS_DSPIC, HIGH); //ensure ss in high
    spiMapleCustom.begin(); //INIT SPI PORT
    spiMapleCustom.setClockDivider(SPI_CLOCK_DIV8); //SET SPI CLOCK 72/8 = 9MHZ
    spiMapleCustom.setBitOrder(MSBFIRST); //SET MSB FIRST
    spiMapleCustom.setDataMode(SPI_MODE1);   // CLK IDLE LOW, DATA CAPTURE ON 2ND TRANS
    delay(10);

}


/**
 * setDspicCommOrder()
 * Fonction de mis en place de l'ordre envoyé sur le dsPIC.
 * Cette fonction peut être modifié à tout moment.
 * (Voir fonction setup pour exemple)
 * @param stopState
 * @param enableState
 * @param torqueState
 * @param pwmBridgeState
 * @param qeiState
 * @param absoluState
 * @param currentState
 * @param voutState
 * @param tempBridgeState
 * @param tempMotor
 * @param tempBatt
 * @param vBattState
 * @param pwmFan1State
 * @param pwmFan2State
 * @param errorState
 * @param configState
 */
void DspicCom::setDspicCommOrder(int stopState,
                       int enableState,
                       int torqueState,
                       int pwmBridgeState,
                       int qeiState,
                       int absoluState,
                       int currentState,
                       int voutState,
                       int tempBridgeState,
                       int tempMotor,
                       int tempBatt,
                       int vBattState,
                       int pwmFan1State,
                       int pwmFan2State,
                       int errorState,
                       int configState){

    glob.ORDER = (uint16)(
            (stopState<<SHT_STOP) |
            (enableState<<SHT_ENABLE) |       
            (torqueState<<SHT_Torque) |
            (pwmBridgeState<<SHT_PWM_Bridge) |
            (qeiState<<SHT_QEI) |
            (absoluState<<SHT_Absolu) |
            (currentState<<SHT_Curent_Bridge) |
            (voutState<<SHT_Vout) |
            (tempBridgeState<<SHT_Temp_Bridge) |
            (tempMotor<<SHT_Temp_Motor) |
            (tempBatt<<SHT_Temp_Bat) |
            (vBattState<<SHT_V_Bat) |
            (pwmFan1State<<SHT_PWM_Fan1) |
            (pwmFan2State<<SHT_PWM_Fan2) |
            (errorState<<SHT_Error) |
            (configState<<SHT_Config));

}

/**
 * communicationWithDspic()
 * Fonction de communication mapleMini<=> dsPIC.
 * Doit être appelé à chaque tour de boucle.
 * Suivant l'ordre définit, la maple envoie et recoit les données requises, et met à jour sa structure de données interne.
 */
void DspicCom::communicationWithDspic(){

    uint16 order = glob.ORDER;

    //int stopState = (order>>SHT_STOP)&0x0001;
    //int enableState = (order>>SHT_ENABLE)&0x0001;
    int torqueState = (order>>SHT_Torque)&0x0001;
    int pwmBridgeState = (order>>SHT_PWM_Bridge)&0x0001;
    int qeiState = (order>>SHT_QEI)&0x0001;
    int absoluState = (order>>SHT_Absolu)&0x0001;
    int currentState = (order>>SHT_Curent_Bridge)&0x0001;
    int voutState = (order>>SHT_Vout)&0x0001;
    int tempBridgeState = (order>>SHT_Temp_Bridge)&0x0001;
    int tempMotor = (order>>SHT_Temp_Motor)&0x0001;
    int tempBatt = (order>>SHT_Temp_Bat)&0x0001;
    int vBattState = (order>>SHT_V_Bat)&0x0001;
    int pwmFan1State = (order>>SHT_PWM_Fan1)&0x0001;
    int pwmFan2State = (order>>SHT_PWM_Fan2)&0x0001;
    int errorState = (order>>SHT_Error)&0x0001;
    int configState = (order>>SHT_Config)&0x0001;

    digitalWrite(SS_DSPIC, LOW); //Low SS to send data

    sendData(order);

    delayMicroseconds(DELAY_AFTER_ORDER);

    if(torqueState)
        glob.torque=receiveData();
    if(pwmBridgeState)
        sendSignedData(glob.pwmPotar);
    if(qeiState)
        glob.qei=receiveData();
    if(absoluState)
        glob.codeurMagn=receiveData();
    if(currentState)
        glob.currentBridge=receiveData();
    if(voutState)
        glob.vout=receiveData();
    if(tempBridgeState)
        glob.tempBridge=receiveData();
    if(tempMotor)
        glob.tempMotor=receiveData();
    if(tempBatt)
        glob.tempBatt=receiveData();
    if(vBattState)
        glob.vBatt=receiveData();
    if(pwmFan1State)
        sendData(glob.pwmFan1);
    if(pwmFan2State)
        sendData(glob.pwmFan2);
    if(errorState)
        glob.error=receiveData();
    if(configState)
        sendData(glob.config);

    delayMicroseconds(DELAY_AFTER_COMM);

    digitalWrite(SS_DSPIC, HIGH);   // take the SS pin high to de-select the chip:
}

/**
 * sendData()
 * Fonction qui envoie un mot de 16bits sur le dsPIC.
 * Un delay est rajouté pour laisser le temps au dsPIC de traiter la donnée recu.
 * @param data
 */
void DspicCom::sendData(uint16 data){

    uint8 dataLow = (data & 0xFF);
    uint8 dataHigh = (data >> 8);

    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)

    delayMicroseconds(DELAY_BETWEEN_COMMAND);
}

/**
 * sendData()
 * Fonction qui envoie un mot de 16bits sur le dsPIC.
 * Un delay est rajouté pour laisser le temps au dsPIC de traiter la donnée recu.
 * @param data
 */
void DspicCom::sendSignedData(int16 data){

    int8 dataLow = (data & 0xFF);
    int8 dataHigh = (data >> 8);

    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)

    delayMicroseconds(DELAY_BETWEEN_COMMAND);
}


/**
 * receiveData()
 * Fonction qui recoit un mot de 16bits envoyé par le dsPIC.
 * Un delay est rajouté pour laisser le temps au dsPIC de se remettre en position SPI.
 * @return
 */
uint16 DspicCom::receiveData(){
    uint8 receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    uint8 receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    return (receivedDataH << 8) | (receivedDataL & 0xff); //On combine les morceaux pour avoir du 16 bits
}