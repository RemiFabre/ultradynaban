/*
 * DspicCom.cpp
 *
 */

#include "DspicCom.h"


DspicCom::DspicCom(uint8 pin_SS) : spiMaple(1)
{
	m_spi_SS_pin = pin_SS;
	pinMode(pin_SS, OUTPUT);
}

DspicCom::DspicCom(uint8 pin_SS, uint32 spi_number) : spiMaple(spi_number)
{
	m_spi_SS_pin = pin_SS;
	pinMode(pin_SS, OUTPUT);
}

DspicCom::~DspicCom()
{
}

void DspicCom::Init()
{
	digitalWrite(m_spi_SS_pin, HIGH); //ensure ss in high
	spiMaple.begin(); //INIT SPI PORT
	spiMaple.begin(SPI_9MHZ, MSBFIRST, SPI_MODE_1);
	delay(10);
}

void DspicCom::SetDefaultOrder(uint16 order)
{
	m_default_order = order;
}

void DspicCom::Refresh()
{
	RefreshData(m_default_order);
}

void DspicCom::Refresh(uint16 order)
{
	RefreshData(order);
}

void DspicCom::RefreshData(uint16 order)
{
	int torqueState = (order >> SHT_Torque) & 0x0001;
	int pwmBridgeState = (order >> SHT_PWM_Bridge) & 0x0001;
	int qeiState = (order >> SHT_QEI) & 0x0001;
	int absoluState = (order >> SHT_Absolu) & 0x0001;
	int currentState = (order >> SHT_Curent_Bridge) & 0x0001;
	int voutState = (order >> SHT_Vout) & 0x0001;
	int tempBridgeState = (order >> SHT_Temp_Bridge) & 0x0001;
	int tempMotor = (order >> SHT_Temp_Motor) & 0x0001;
	int tempBatt = (order >> SHT_Temp_Bat) & 0x0001;
	int vBattState = (order >> SHT_V_Bat) & 0x0001;
	int pwmFan1State = (order >> SHT_PWM_Fan1) & 0x0001;
	int pwmFan2State = (order >> SHT_PWM_Fan2) & 0x0001;
	int errorState = (order >> SHT_Error) & 0x0001;
	int configState = (order >> SHT_Config) & 0x0001;

	digitalWrite(m_spi_SS_pin, LOW); //Low SS to send data

	//Send the Order, then wait, to let the dsPIC prepare datas to send.
	SendData(order);
	delayMicroseconds(DELAY_AFTER_ORDER);

	//Get all the data request from the order, and send data if necessary.
	if (torqueState)
		m_torque = ReceiveData();
	if (pwmBridgeState)
		SendSignedData(m_pwmBridge);
	if (qeiState)
		m_qei = ReceiveData();
	if (absoluState)
		m_codeurMagn = ReceiveData();
	if (currentState)
		m_currentBridge = ReceiveData();
	if (voutState)
		m_vout = ReceiveData();
	if (tempBridgeState)
		m_tempBridge = ReceiveData();
	if (tempMotor)
		m_tempMotor = ReceiveData();
	if (tempBatt)
		m_tempBatt = ReceiveData();
	if (vBattState)
		m_vBatt = ReceiveData();
	if (pwmFan1State)
		SendData(m_pwmFan1);
	if (pwmFan2State)
		SendData(m_pwmFan2);
	if (errorState)
		m_error = ReceiveData();
	if (configState)
		SendData(m_config);

	delayMicroseconds(DELAY_AFTER_COMM);

	digitalWrite(m_spi_SS_pin, HIGH);   // take the SS pin high to de-select the chip:
}

void DspicCom::SendData(uint16 data)
{
	uint8 dataLow = (data & 0xFF);
	uint8 dataHigh = (data >> 8);

	spiMaple.transfer(dataHigh); //send data via spi (MSB)
	spiMaple.transfer(dataLow); //send data via spi (LSB)

	delayMicroseconds(DELAY_BETWEEN_COMMAND);
}

void DspicCom::SendSignedData(int16 data)
{
	int8 dataLow = (data & 0xFF);
	int8 dataHigh = (data >> 8);

	spiMaple.transfer(dataHigh); //send data via spi (MSB)
	spiMaple.transfer(dataLow); //send data via spi (LSB)

	delayMicroseconds(DELAY_BETWEEN_COMMAND);
}

uint16 DspicCom::ReceiveData()
{
	uint8 receivedDataH = spiMaple.transfer(0x00); //Get the first part (8bits)
	uint8 receivedDataL = spiMaple.transfer(0x00); //Get the second part (8bits)
	delayMicroseconds(DELAY_BETWEEN_COMMAND);
	return (receivedDataH << 8) | (receivedDataL & 0xff); //Combine the two parts to get a 16bits value
}

//Setter Getter

void DspicCom::SetPwmBridge(int16 pwm)
{
	m_pwmBridge = pwm;
}
void DspicCom::SetPwmFan1(uint16 pwm)
{
	m_pwmFan1 = pwm;
}
void DspicCom::SetPwmFan2(uint16 pwm)
{
	m_pwmFan2 = pwm;
}
void DspicCom::SetConfig(uint16 config)
{
	m_config = config;
}
uint16 DspicCom::GetTorque()
{
	return m_torque;
}
uint16 DspicCom::GetInternalCodeur()
{
	return m_qei;
}
uint16 DspicCom::GetMagneticCodeur()
{
	return m_codeurMagn;
}
uint16 DspicCom::GetBridgeCurrent()
{
	return m_currentBridge;
}
int16 DspicCom::GetBridgeVoltage()
{
	return m_vout;
}
uint16 DspicCom::GetBridgeTemp()
{
	return m_tempBridge;
}
uint16 DspicCom::GetMotorTemp()
{
	return m_tempMotor;
}
uint16 DspicCom::GetBatteryTemp()
{
	return m_tempBatt;
}
uint16 DspicCom::GetBatteryVoltage()
{
	return m_vBatt;
}
uint16 DspicCom::GetErrors()
{
	return m_error;
}