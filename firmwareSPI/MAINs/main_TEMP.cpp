#include <libmaple/libmaple_types.h>
#include <libmaple/spi.h>
#include <libmaple/dma.h>

#include <boards.h>
#include <stdint.h>

#include <wirish.h>
#include <wirish/wirish.h>

#include <libmaple/timer.h>
#include <libmaple/util.h>
#include <libmaple/rcc.h>

#include <libmaple/adc.h>
#include <libmaple/timer.h>

/* On attend une donnée du port série, on se met à la position home (closed loop), puis on a attend une donnée de pwm via serial
	On fait 1 test à 1kHz et on envoi un tableau de 1000 points sur le port série. On refait 5 fois la mesure puis on attend une nouvelle
	valeur de pwm
*/


#define SS_DSPIC PA4   //ss
#define ORDER_SHORT 0x2E00//torque, pos1,pos2,current
#define ORDER_LONG 0x2FF0//every sensors

#define DELAY_AFTER_ORDER 8
#define DELAY_BETWEEN_COMMAND 5
#define MAX_ANGLE_ABS 4095
#define MAX_PWM 2045
#define idle_state 3 // mode d'attente
#define ctrl_pos_state 0 //mode controle position
#define mesure_state 1 //mode mesure
#define data_send_state 2 //mode envoi de data
#define max_error_angle_home 5
#define max_error_angle_step 100
#define rapport_abs_relative 157 //rapport de réduction entre absolu et relative
#define K_P 2.5 // gain P
#define K_I 0//gain I
#define SAMPLING_PERIOD 0.001
#define CENTER_OFFSET 318485
#define ANGLE_STEP 250000
#define ANGLE_HOME 160679
#define MAX_I_CONTRIB 1000
#define LIM_CW_ANGLE 260000
#define AMPLITUDE 30000
#define freq 0.2


unsigned int torque_BIN,current_BIN,temp_bridge_BIN,temp_bat_BIN,temp_mot_BIN,v_bat_BIN; //variables capteurs
signed int v_out_BIN;
unsigned short tick_short =0,tick_long =0 ;
unsigned short receivedDataH,receivedDataL,dataLow,dataHigh;
HardwareTimer t_2K(1);
float tempSerial;
float rapport=((3.0)/4095.0);
float rapportBeeg=((3.0)/65535);
float rapportCodMagn=((360.0)/4095.0);
float rapportPourCent=((100.0)/4095.0);
HardwareSPI spiMapleCustom(1);
long int timer_cnt_long;
int pwm_bridge=0;
long int t=0, t_old=0,time_start=0, time_loop=0, loop_cnt;
unsigned int abs_pos_BIN;
uint16 inc_cnt_BIN ;
uint32 relative_pos=0;
uint8 state ;
uint32 max_angle_inc = 642715 ; //valeur de l'encodeur pour un tour de l'articulation (157*4095)
uint32 angle_inc ;
int32 current_error_angle;
int timer_cnt=0;
uint16 incremental_offset ;
uint8 nb_mesure =5;
int16 pwm_value_from_serial ;
float ang_velocity;
short int i,cnt;
uint16 mean_temp_mot=0,mean_temp_bridge=0,mean_current=0,mean_temp_bat=0;
uint16 cnt_10hz=0;
int32 error_angle;
uint32 target_angle;
char buffer[100];
int log_index =0 ;
float target_pos=0;
bool sent=false;
const float return_pos = 0;

void send_pwm (int value_pwm);
void tick_timer ();
void handle_serial ();
void receive_data_long();
void receive_data_short();
void initSPI_dspic();
void rampe_1s (int value);
void dspic_enable();
void dspic_stop();
void toursQeiRefresh();
int32 control_angle_diff(uint32 a, uint32 b);
void tick_asserv(int32 error,float k_p,float k_i);
void mesure();
uint32 angle_inc_calc (uint32 val_inc,uint32 max_angle);
void send_data(int cnt);
float angular_velocity_calc(int32 pos, float sampling_time);
void ctrlSinus();
void go_pos(float angle_deg) ; //angle en degrés


#define nb_points 1000
//Tableau pour le transfert serie
uint16 temp_mot_data[nb_points];
uint16 temp_bridge_data[nb_points];
uint16 current_data[nb_points];
uint32 angle_data[nb_points];
int16 pwm_data[nb_points];

void setup() {
    t_2K.pause();
    pinMode(SS_DSPIC,OUTPUT); //set slave select as output
    Serial1.begin(1000000);
    // SPI INIT //
    initSPI_dspic() ;
    // TIMER INIT //
    t_2K.setPeriod(1000); //fs = 1kHz
    t_2K.setChannel1Mode(TIMER_OUTPUT_COMPARE);
    t_2K.setCompare(TIMER_CH1, 1);  // Interrupt 1 count after each update
    t_2K.attachCompare1Interrupt(tick_timer);
    t_2K.refresh();
    // Start the timer counting
    t_2K.resume();
    //on attend le premier mot en serial pour lancer la loop
    while(!Serial1.available()) {};
    Serial1.read(); //dummy read to clear serial buffer
    time_start=millis();
    dspic_enable();
    receive_data_short();

    incremental_offset = inc_cnt_BIN;
    relative_pos=abs_pos_BIN*157-CENTER_OFFSET ; //On prend la position de départ par rapport à l'encodeur absolu
    //on enlève offset pour mettre le Zero au niveau du moteur (zone morte)
    state=ctrl_pos_state; // on part par défaut en controle de position
    delay(50);

    for (int i=0;i<nb_points;i++) {
        temp_mot_data[i]=0;
        temp_bridge_data[i]=0;
        current_data[i]=0;
        angle_data[i]=0;
    }

}

void loop() {
    if (tick_short) { //actualisation des valeurs
        receive_data_short();
        tick_short = 0;
        toursQeiRefresh();
        angle_inc=angle_inc_calc(relative_pos, max_angle_inc);
        t++;
        if (log_index==nb_points) {
            go_pos(target_pos);
            current_error_angle = control_angle_diff(angle_inc,(target_pos * max_angle_inc / 360+ANGLE_HOME));
            if (abs(current_error_angle) < 1000)  //end of measure
            {
                log_index = 0;
                if (!sent) {
                    for (int i = 0; i < nb_points; i++) {
                        Serial1.print(((current_data[i] * (3.0 / 4096)) - 1.5185) * (20.0 / 0.625));
                        Serial1.print("\t");
                        Serial1.print(temp_mot_data[i] * 300.0 / 4096);
                        Serial1.print("\t");
                        Serial1.print(temp_mot_data[i] * 300.0 / 4096);
                        Serial1.print("\t");
                        Serial1.print(pwm_data[i] * 100.0 / 2048);
                        Serial1.print("\t");
                        Serial1.println(angle_data[i]);

                    }
                }
                sent=true;

            }
        }
        else
        {
            go_pos(target_pos);
        }
        send_pwm(pwm_bridge);
    }

    if (tick_long) {
        receive_data_long();
        tick_long=0;
        if (log_index<nb_points)
        {
            if (target_pos!=return_pos)
            {
                temp_mot_data[log_index] = temp_mot_BIN ;
                temp_bridge_data[log_index] = temp_bridge_BIN;
                current_data[log_index] = current_BIN;
                angle_data[log_index] = angle_inc;
                pwm_data[log_index] = pwm_bridge;
                log_index++;
            }
            if (log_index==nb_points)
            {
                target_pos=return_pos;
            }
        }

    }


    if (Serial1.available()&&(nb_points!=log_index)) {
        target_pos=Serial1.read();//Serial1.parseFloat();
        Serial1.println(target_pos);
    }



}

void tick_timer () { //génère un tick @1kHz & 100Hz
    timer_cnt++;
    timer_cnt_long ++ ;
    if (timer_cnt>=5) // ~100Hz
    {
        timer_cnt=0;
        tick_long = 1;
    }
    else {
        tick_short = 1;
    }
}

void send_pwm (int value_pwm) {
    /*ENVOI ORDRE PWM */
    dataLow = (0x1000 & 0xFF); //ordre --> PWM
    dataHigh = (0x1000 >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    uint8 dataLow = (value_pwm & 0xFF);
    uint8 dataHigh = (value_pwm >> 8);
    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    digitalWrite(SS_DSPIC, HIGH);
}

void handle_serial() {
    Serial1.println("===========================");
    Serial1.print("Couple : \t\t");
    tempSerial=((torque_BIN*rapportBeeg-1.5)/0.061);
    Serial1.print(tempSerial,4);
    Serial1.println(" Nm");
    Serial1.print("Position Absolue : \t");
    tempSerial=(abs_pos_BIN*rapportCodMagn);
    Serial1.print(tempSerial,2);
    Serial1.println(" Deg");
    Serial1.print("Position Relative : \t");
    Serial1.print(inc_cnt_BIN);
    Serial1.println("");
    Serial1.print("Tension Entrée : \t");
    tempSerial=(v_bat_BIN*rapport)*10;
    Serial1.print(tempSerial,2);
    Serial1.println(" Volts");
    Serial1.print("Température Pont : \t");
    tempSerial=(temp_bridge_BIN*rapport)*100;
    Serial1.print(tempSerial,1);
    Serial1.println(" deg C");
    Serial1.print("Courant Sortie : \t");
    tempSerial=((current_BIN*rapport-1.5174)*20.0)/0.625;
    Serial1.print(tempSerial,4);
    Serial1.println(" A");
    Serial1.print("Tension Sortie : \t");
    tempSerial=(v_out_BIN*rapport)*10;
    Serial1.print(tempSerial,2);
    Serial1.println(" Volts");
    Serial1.print("Température 1 : \t");
    tempSerial=(temp_bat_BIN*rapport)*100;
    Serial1.print(tempSerial,1);
    Serial1.println(" deg C");
    Serial1.print("Température 2 : \t");
    tempSerial=(temp_mot_BIN*rapport)*100;
    Serial1.print(tempSerial,1);
    Serial1.println(" deg C");
    Serial1.println("");
    Serial1.print("temps : \t");
    tempSerial=t/1000.0;
    Serial1.print(tempSerial,2);
    Serial1.println(" s");
    Serial1.println("");
    Serial1.print("Commande PWM : \t");
    Serial1.print(pwm_bridge);
    Serial1.println("");

}

void receive_data_long() {
    /*ENVOI ORDRE LONG */
    digitalWrite(SS_DSPIC, LOW);
    dataLow = (ORDER_LONG & 0xFF);
    dataHigh = (ORDER_LONG >> 8);
    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    /*RECUP VALEURS*/
    /*torque*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    torque_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*INC POS*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    inc_cnt_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*ABS POS*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    abs_pos_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*CURRENT*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    current_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*Vout*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    v_out_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*temp_bridge*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    temp_bridge_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*temp_mot*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    temp_mot_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*temp_bat*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    temp_bat_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*v_bat*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    v_bat_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    digitalWrite(SS_DSPIC, HIGH);
}

void receive_data_short(){
    /*ENVOI ORDRE SHORT */
    dataLow = (ORDER_SHORT & 0xFF);
    dataHigh = (ORDER_SHORT >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    /*RECUP VALEURS*/
    /*torque*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    torque_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*INC POS*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    inc_cnt_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*ABS POS*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    abs_pos_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*CURRENT*/
    receivedDataH = spiMapleCustom.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spiMapleCustom.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    current_BIN = (receivedDataH << 8) | (receivedDataL & 0xff);
    digitalWrite(SS_DSPIC, HIGH);
}

void initSPI_dspic(){
    digitalWrite(SS_DSPIC, HIGH); //ensure ss in high
    spiMapleCustom.begin(SPI_9MHZ, MSBFIRST, SPI_MODE_1);
    /*spiMapleCustom.begin(); //INIT SPI PORT
    spiMapleCustom.setClockDivider(SPI_CLOCK_DIV8); //SET SPI CLOCK 72/8 = 9MHZ
    spiMapleCustom.setBitOrder(MSBFIRST); //SET MSB FIRST
    spiMapleCustom.setDataMode(SPI_MODE1);   // CLK IDLE LOW, DATA CAPTURE ON 2ND TRANS
    */
    delay(10);
}

void rampe_1s (int target) { //génére une rampe de 0 à 100% en ~1s (NE MARCHE QU'EN POSITIF !!!)
    if (pwm_bridge<=target) {
        pwm_bridge=pwm_bridge+1;
    }
}

void dspic_enable() {
    dataLow = (0x4000 & 0xFF); //ordre --> PWM
    dataHigh = (0x4000 >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    digitalWrite(SS_DSPIC, HIGH);
}

void dspic_stop() {

    dataLow = (0x8000 & 0xFF); //ordre --> PWM
    dataHigh = (0x8000 >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spiMapleCustom.transfer(dataHigh); //send data via spi (MSB)
    spiMapleCustom.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    digitalWrite(SS_DSPIC, HIGH);
}

/**
 * toursQeiRefresh()
 * Fonction qui raffraichie le QEI recu du dsPIC.
 * On utilise une varibale 32 bits interne.
 */
void toursQeiRefresh(){

    static uint16 anciennevaleurQEI=0; //Static => est reservé pour la fonction
    uint16 nouvellevaleurQEI=(inc_cnt_BIN-incremental_offset);
    int16 dq=nouvellevaleurQEI-anciennevaleurQEI;

    if(dq>32000){
        dq=dq-65536;//depassement negatif
    }
    else if(dq<-32000){
        dq=dq+65536;
    }

    relative_pos=relative_pos+dq;

    anciennevaleurQEI=nouvellevaleurQEI;
}

/**
 * control_angle_diff()
 * Fonction qui renvoie la plus petite différence entre 2 points.
 * @param a
 * @param b
 * @return
 */
int32 control_angle_diff(uint32 a, uint32 b) {
    int32 diff = a - b;
    int32 limit = (max_angle_inc + 1) / 2;

    if (diff > limit) {
        return diff - (max_angle_inc + 1);
    }
    if (diff < -limit) {
        return diff + (max_angle_inc + 1);
    }

    return diff;
}

/**
 * tick_asserv()
 * fonction qui asservie la PWM en fonction de la position actuelle et de la position voulue.
 * @param pos
 * @param target
 * @param k_p (gain)
 */
void tick_asserv(int32 error,float k_p,float k_i){

    static float i_contrib=0;
    float p_contrib=error*k_p;
    i_contrib=i_contrib+(error*SAMPLING_PERIOD)*k_i;
    if (i_contrib > MAX_I_CONTRIB)
        i_contrib = MAX_I_CONTRIB ;
    else if (i_contrib < -MAX_I_CONTRIB)
        i_contrib = -MAX_I_CONTRIB ;
    int32 feedback_gain=p_contrib+i_contrib;
    if(feedback_gain<-MAX_PWM)
        feedback_gain=-MAX_PWM; //ERREUR : n'aime pas le -2048, attention
    else if(feedback_gain>MAX_PWM)
        feedback_gain=MAX_PWM;

    pwm_bridge=feedback_gain;
}

/**
 * mesure()
 * Fonction qui realise une acquisition et qui stock la valeur dans un tableau à l'indice cnt
 * @param cnt
 */
void mesure () {
    i++;
    if(i>1)
    {
        cnt++;
        current_data[cnt] = current_BIN; //on recupere dans tableau les valeurs
        //abs_pos[cnt] = abs_pos_BIN;
        //angle_inc_data[cnt] = angle_inc;
        delayMicroseconds(10);
        i=0;
    }
}

/**
 * angle_inc_calc ()
 * Fonction qui calcul la position de l'articulation modulo un angle_max
 * @param val_inc
 * @param max_angle
 */
uint32 angle_inc_calc (uint32 val_inc,uint32 max_angle) {
    uint32 angle_inc;

    if (val_inc>10*max_angle) {
        angle_inc=max_angle-(4294967295-val_inc);
        return (angle_inc) ;
    }

    else if (val_inc>max_angle) {
        angle_inc=val_inc-max_angle;
        return (angle_inc) ;
    }

    else if (val_inc<0){
        angle_inc=val_inc+max_angle;
        return (angle_inc);
    }

    else {
        angle_inc=val_inc;
        return (angle_inc);
    }

}

/**
 * send_data ()
 * Fonction qui envoi sur le port série la valeur d'indice cnt de plusieurs tableaux
 * @param cnt
 */
void send_data(int cnt) {
    Serial1.print(((current_data[cnt]*(3.0/4095))-1.5185)*(20.0/0.625));
    Serial1.print("\t");
    //  Serial1.print(abs_pos[cnt]*(360.0/4095));
    // Serial1.print("\t");
    //Serial1.print(velocity_data[cnt]);
    Serial1.print("1000");
    Serial1.print("\t");
    //Serial1.println(angle_inc_data[cnt]);
}


void ctrlSinus(){
    target_angle=(int32)(AMPLITUDE*sin(2*3.1415*freq*(t)/1000)+ANGLE_HOME);
    error_angle=control_angle_diff(angle_inc,target_angle);
    tick_asserv(error_angle, K_P,K_I);
}

void go_pos(float angle_deg) {
    target_angle=ANGLE_HOME+uint32(angle_deg*max_angle_inc/360);
    error_angle=control_angle_diff(angle_inc,target_angle);
    tick_asserv(error_angle, K_P,K_I);
}
