#include <wirish/wirish.h>
#include <libmaple/adc.h>
#include <libmaple/timer.h>
#include "dxl_HAL.h"
#include "motor.h"
#include "control.h"
#include "dxl.h"
#include "trajectory_manager.h"
#include "flash_write.h"


#define SS_DSPIC PA4   //ss
#define RESET_DSPIC PC15

#define SHT_STOP            15
#define SHT_ENABLE          14
#define SHT_Torque          13
#define SHT_PWM_Bridge      12
#define SHT_QEI             11
#define SHT_Absolu          10
#define SHT_Curent_Bridge   9
#define SHT_Vout            8
#define SHT_Temp_Bridge     7
#define SHT_Temp_Motor      6
#define SHT_Temp_Bat        5
#define SHT_V_Bat           4
#define SHT_PWM_Fan1        3
#define SHT_PWM_Fan2        2
#define SHT_Error           1
#define SHT_Config          0

#define DELAY_BETWEEN_COMMAND 5
#define DELAY_AFTER_ORDER 8
#define DELAY_AFTER_COMM 2

#define ORDER_SHORT 0x2E00//torque, pos1,pos2,current
#define ORDER_LONG 0x2FF0//every sensors

#define MEASURES_SIZE 450

/**
 * Schedules the hardware tasks
 */
void hardware_tick();

void set_ready_to_update_hardware();

int my_atoi(char *p, bool * success);

void initSPI();
void dspic_enable();
void dspic_stop();
void receive_data_short();
void print_pos();
void read_hardware(bool is_first_read=false);
void set_pwm(int16 v);
void communicationWithDspic(uint16 order);
void sendData(uint16 data);
void sendSignedData(int16 data);

uint16 receiveData();

uint16 measures[MEASURES_SIZE][7];

long           counter               = 0;
unsigned int   hardwareCounter       = 0;
bool           readyToUpdateHardware = false;
int index = 0;
HardwareTimer timer2(2);
encoder * encoder0;
unsigned char  controlMode           = OFF;
hardware       hardwareStruct;
char led_value = 0;
bool led_on = false;
uint32 time_ms = 0;
HardwareSPI spi(1);
bool stop = false;
// SPI1 PA7=MOSI, PA6=MISO, PA5=SCK, PA4=NSS
// Pins on the maple mini: PA7=4, PA6=5, PA5=6, PA4=7

void setup() {
    disableDebugPorts();

    //afio_remap(AFIO_REMAP_USART1);
    //gpio_set_mode(GPIOB, 6, GPIO_AF_OUTPUT_PP);
    //gpio_set_mode(GPIOB, 7, GPIO_INPUT_FLOATING);
    pinMode(BOARD_LED_PIN, OUTPUT);
    pinMode(RESET_DSPIC, OUTPUT);

    digitalWrite(RESET_DSPIC, LOW); // Resetting the dspic
    delay(50);
    digitalWrite(RESET_DSPIC, HIGH);
    delay(150);
    
    timer2.pause();
    // The hardware will be read at 1Khz
    timer2.setPrescaleFactor(72);
    timer2.setOverflow(1000);
    timer2.setChannel1Mode(TIMER_OUTPUT_COMPARE);
    // Interrupt 1 count after each update
    timer2.setCompare(TIMER_CH1, 1);
    timer2.attachCompare1Interrupt(set_ready_to_update_hardware);
    timer2.refresh();
    timer2.resume();
    
    initSPI();
    pinMode(SS_DSPIC, OUTPUT); //set slave select as output
    delay(1);
    dspic_enable();


    // The firs read calls motor_init() with the read values
    read_hardware(true);
    
    //Traj
    //predictive_control_init();

    //Control
    control_init();

    hardwareStruct.mot = motor_get_motor();
    //hardwareStruct.temperature = read_temperature();
    //hardwareStruct.voltage = read_voltage();
    //SerialUSB.println("Initalizaton finished");

    delay(2000);
}


void loop() {
    if (readyToUpdateHardware) {
        counter++;
        readyToUpdateHardware = false;
        hardware_tick();
    }
}

void hardware_tick() {
    if (stop) {
        return;
    }
    //These actions are performed at a rate of 1kHz
    hardwareCounter++;
    time_ms = time_ms + 1;
    //read_hardware();
    uint16 order = (1U<<SHT_QEI) | (1U<<SHT_Absolu) | (1U<<SHT_Curent_Bridge) | (1U<<SHT_Vout) | (1U<<SHT_Temp_Bridge) | (1U<<SHT_Temp_Motor) | (1U<<SHT_Temp_Bat)  | (1U<<SHT_Error);
    communicationWithDspic(order);
    motor_update(hardwareStruct.codeurMagn, hardwareStruct.qei, hardwareStruct.currentBridge);
    motor_set_target_angle(3472);
    //motor_set_target_angle(3472 + 100*sin(2*3.1415*1*time_ms/1000));

    int16 deltaAngle = control_angle_diff(hardwareStruct.mot->targetAngle, hardwareStruct.mot->angle);

    motor_set_command(deltaAngle * 32);

    if (time_ms < 30000) {
        //set_pwm(0);
        set_pwm(hardwareStruct.mot->command);
    } else {
//        set_pwm(0);
//        dspic_stop();
//        SerialUSB.println("time angle command current tempBridge tempMotor tempBatt");
//        for (int i = 0; i < MEASURES_SIZE; i++) {
//            for (int j = 0; j < 7; j++) {
//                SerialUSB.print(measures[i][j]);
//                SerialUSB.print(" ");
//            }
//            SerialUSB.println();
//            stop = true;
//        }
    }
    int ratio = 66;
    /*if (time_ms < 400) {
        ratio = 1;
        }*/
    if (time_ms%ratio == 0) {
        uint32 index = time_ms/ratio;
        if (index < MEASURES_SIZE) {
            measures[index][0] = time_ms;
            measures[index][1] = hardwareStruct.mot->angle;
            measures[index][2] = hardwareStruct.mot->command;
            measures[index][3] = hardwareStruct.mot->current;
            measures[index][4] = hardwareStruct.tempBridge;
            measures[index][5] = hardwareStruct.tempMotor;
            measures[index][6] = hardwareStruct.tempBatt;
        }

    }


    //int16 pwm = 2047*sin(2*3.1415*0.1*time_ms/1000);
    //int16 pwm = ((time_ms/5000)%6)*-400;
    //set_pwm(pwm);
    if (hardwareCounter & (1 << 9)) {
        //receive_data_short();

		SerialUSB.println("==============================");

        SerialUSB.print("position = ");
        SerialUSB.print(hardwareStruct.mot->angle);
        SerialUSB.print(", inc = ");
        SerialUSB.print(hardwareStruct.mot->inc_angle);
        SerialUSB.print(", current = ");
        SerialUSB.print(hardwareStruct.mot->current);
        SerialUSB.print(", error = ");
        SerialUSB.println(hardwareStruct.error);

        SerialUSB.print("vBatt = ");
        SerialUSB.print(hardwareStruct.vBatt);
        SerialUSB.print(", tempBridge = ");
        SerialUSB.print(hardwareStruct.tempBridge);
        SerialUSB.print(", tempMotor = ");
        SerialUSB.print(hardwareStruct.tempMotor);
        SerialUSB.print(", tempBatt = ");
        SerialUSB.println(hardwareStruct.tempBatt);
        SerialUSB.println();

        SerialUSB.print("PWM = ");
        SerialUSB.println(hardwareStruct.mot->command);
        SerialUSB.print("targetAngle = ");
        SerialUSB.println(hardwareStruct.mot->targetAngle);
        SerialUSB.print("deltaAngle = ");
        SerialUSB.println(deltaAngle);
        SerialUSB.println();

        
        //These actions are performed at ~2 Hz
        led_on = ~led_on;
        if (led_on) {
            digitalWrite(BOARD_LED_PIN, LOW);
        } else {
            digitalWrite(BOARD_LED_PIN, HIGH);
        }

        hardwareCounter = 0;
    }
}

void set_ready_to_update_hardware() {
    readyToUpdateHardware = true;
}


// Force init to be called *first*, i.e. before static object allocation.
// Otherwise, statically allocated objects that need libmaple may fail.
__attribute__((constructor)) void premain() {
    init();
}

int main(void) {
    setup();

    while (true) {
        loop();
    }


    return 0;
}


int my_atoi(char *p, bool * success) {
    int k = 0;
    int sign = 1;
    if (*p == '-') {
        sign = -1;
        p++;
    }
    while (*p != '\0') {
        int value = *p - '0';
        if (value >=0 && value <= 9) {
            k = k*10 + value;
            p++;
        } else {
            *success = false;
            return 0;
        }
    }
    *success = true;
    return k*sign;
}

void initSPI(){
    pinMode(SS_DSPIC, OUTPUT); //set slave select as output
    digitalWrite(SS_DSPIC, HIGH); //ensure ss in high
    //spi.begin(SPI_1_125MHZ, MSBFIRST, SPI_MODE_1);
    spi.begin(SPI_9MHZ, MSBFIRST, SPI_MODE_1);
    delay(10);
}

void dspic_enable() {
    uint8 dataLow = (0x4000 & 0xFF); //ordre --> PWM
    uint8 dataHigh = (0x4000 >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spi.transfer(dataHigh); //send data via spi (MSB)
    spi.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    digitalWrite(SS_DSPIC, HIGH);
}

void dspic_stop() {
    uint8 dataLow = (0x8000 & 0xFF); //ordre --> PWM
    uint8 dataHigh = (0x8000 >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spi.transfer(dataHigh); //send data via spi (MSB)
    spi.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    digitalWrite(SS_DSPIC, HIGH);
}

void set_pwm(int16 v) {
    uint16 order = (1U<<SHT_PWM_Bridge);

    digitalWrite(SS_DSPIC, LOW); //Low SS to send data

    sendData(order);

    delayMicroseconds(DELAY_AFTER_ORDER);
    //Astuce !
    sendSignedData(-v);
}

void receive_data_short() {
    uint16 inc_pos = 0;
    uint16 abs_pos = 0;
    uint16 current = 0;
    uint8 dataLow = 0;
    uint8 dataHigh = 0;
    uint8 receivedDataH = 0;
    uint8 receivedDataL = 0;
    
    uint16 order = (1U<<SHT_QEI) | (1U<<SHT_Absolu) | (1U<<SHT_Curent_Bridge);
    dataLow = (order & 0xFF);
    dataHigh = (order >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spi.transfer(dataHigh); //send data via spi (MSB)
    spi.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    /*RECUP VALEURS*/
    /*INC POS*/
    receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    inc_pos = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*ABS POS*/
    receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    abs_pos = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*CURRENT*/
    receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    current = (receivedDataH << 8) | (receivedDataL & 0xff);
    digitalWrite(SS_DSPIC, HIGH);

    
    SerialUSB.println();
    SerialUSB.print("abs_pos = ");
    SerialUSB.print(abs_pos);
    SerialUSB.print(",  ");

    SerialUSB.print("inc_pos = ");
    SerialUSB.print(inc_pos);
    SerialUSB.print(",  ");
    
    SerialUSB.print("current = ");
    SerialUSB.println(current);
    SerialUSB.println();
    
}


void print_pos() {
    uint16 abs_pos = 0;
    uint8 dataLow = 0;
    uint8 dataHigh = 0;
    uint8 receivedDataH = 0;
    uint8 receivedDataL = 0;
    
    uint16 order = 1<<SHT_Absolu;
    dataLow = (order & 0xFF);
    dataHigh = (order >> 8);
    digitalWrite(SS_DSPIC, LOW);
    
    spi.transfer(dataHigh); //send data via spi (MSB)
    spi.transfer(dataLow); //send data via spi (LSB)

    delayMicroseconds(DELAY_AFTER_ORDER);
    /*RECUP VALEURS*/
    /*ABS POS*/
    receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    abs_pos = (receivedDataH << 8) | (receivedDataL & 0xff);
    digitalWrite(SS_DSPIC, HIGH);

    SerialUSB.println();
    SerialUSB.print("abs_pos = ");
    SerialUSB.print(abs_pos);
}

/*#define SHT_Vout            8
#define SHT_Temp_Bridge     7
#define SHT_Temp_Motor      6
#define SHT_Temp_Bat        5
#define SHT_V_Bat           4*/
void read_hardware(bool is_first_read) {
    uint16 inc_pos = 0;
    uint16 abs_pos = 0;
    uint16 current = 0;
    uint8 dataLow = 0;
    uint8 dataHigh = 0;
    uint8 receivedDataH = 0;
    uint8 receivedDataL = 0;
    
    uint16 order = (1U<<SHT_QEI) | (1U<<SHT_Absolu) | (1U<<SHT_Curent_Bridge);
    dataLow = (order & 0xFF);
    dataHigh = (order >> 8);
    digitalWrite(SS_DSPIC, LOW);
    spi.transfer(dataHigh); //send data via spi (MSB)
    spi.transfer(dataLow); //send data via spi (LSB)
    delayMicroseconds(DELAY_AFTER_ORDER);
    /*RECUP VALEURS*/
    /*INC POS*/
    receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    inc_pos = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*ABS POS*/
    receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    abs_pos = (receivedDataH << 8) | (receivedDataL & 0xff);
    /*CURRENT*/
    receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    current = (receivedDataH << 8) | (receivedDataL & 0xff);
    digitalWrite(SS_DSPIC, HIGH);

    // Attention ! Putting an uint16 into an int16. Works cos the range is [0, 4095]
    // To be fixed
    if (is_first_read) {
        motor_init(abs_pos, inc_pos);
    } else {
        motor_update(abs_pos, inc_pos, current);
    }
}


void communicationWithDspic(uint16 order) {
    int torqueState = (order>>SHT_Torque)&0x0001;
    int qeiState = (order>>SHT_QEI)&0x0001;
    int absoluState = (order>>SHT_Absolu)&0x0001;
    int currentState = (order>>SHT_Curent_Bridge)&0x0001;
    int voutState = (order>>SHT_Vout)&0x0001;
    int tempBridgeState = (order>>SHT_Temp_Bridge)&0x0001;
    int tempMotor = (order>>SHT_Temp_Motor)&0x0001;
    int tempBatt = (order>>SHT_Temp_Bat)&0x0001;
    int vBattState = (order>>SHT_V_Bat)&0x0001;
    int errorState = (order>>SHT_Error)&0x0001;

    digitalWrite(SS_DSPIC, LOW); //Low SS to send data

    sendData(order);

    delayMicroseconds(DELAY_AFTER_ORDER);

    if(torqueState)
        hardwareStruct.torque=receiveData();
    if(qeiState)
        hardwareStruct.qei=receiveData();
    if(absoluState)
        hardwareStruct.codeurMagn=receiveData();
    if(currentState)
        hardwareStruct.currentBridge=receiveData();//(receiveData()*3/4096.0 -1.5181)*200/0.625;
    if(voutState)
        hardwareStruct.vout=receiveData();
    if(tempBridgeState)
        hardwareStruct.tempBridge=receiveData()*3*1000/4096;
    if(tempMotor)
        hardwareStruct.tempMotor=receiveData()*3*1000/4096;;
    if(tempBatt)
        hardwareStruct.tempBatt=receiveData()*3*1000/4096;;
    if(vBattState)
        hardwareStruct.vBatt=receiveData();
    if(errorState)
        hardwareStruct.error=receiveData();

    delayMicroseconds(DELAY_AFTER_COMM);

    digitalWrite(SS_DSPIC, HIGH);   // take the SS pin high to de-select the chip:
}

/**
 * sendData()
 * Fonction qui envoie un mot de 16bits sur le dsPIC.
 * Un delay est rajouté pour laisser le temps au dsPIC de traiter la donnée recu.
 * @param data
 */
void sendData(uint16 data){

    uint8 dataLow = (data & 0xFF);
    uint8 dataHigh = (data >> 8);

    spi.transfer(dataHigh); //send data via spi (MSB)
    spi.transfer(dataLow); //send data via spi (LSB)

    delayMicroseconds(DELAY_BETWEEN_COMMAND);
}

/**
 * sendData()
 * Fonction qui envoie un mot de 16bits sur le dsPIC.
 * Un delay est rajouté pour laisser le temps au dsPIC de traiter la donnée recu.
 * @param data
 */
void sendSignedData(int16 data){

    int8 dataLow = (data & 0xFF);
    int8 dataHigh = (data >> 8);

    spi.transfer(dataHigh); //send data via spi (MSB)
    spi.transfer(dataLow); //send data via spi (LSB)

    delayMicroseconds(DELAY_BETWEEN_COMMAND);
}


/**
 * receiveData()
 * Fonction qui recoit un mot de 16bits envoyé par le dsPIC.
 * Un delay est rajouté pour laisser le temps au dsPIC de se remettre en position SPI.
 * @return
 */
uint16 receiveData(){
    uint8 receivedDataH = spi.transfer(0x00); //On récupère le premier morceau de 8 bits
    uint8 receivedDataL = spi.transfer(0x00); //On récupère le second morceau de 8 bits
    delayMicroseconds(DELAY_BETWEEN_COMMAND);
    return (receivedDataH << 8) | (receivedDataL & 0xff); //On combine les morceaux pour avoir du 16 bits
}
