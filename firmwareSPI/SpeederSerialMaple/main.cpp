#include <iostream>
#include "SerialPort.h"
#include <stdio.h>
#include <string.h>
#include <Windows.h>
#include <sys/time.h>

#define TAILLE_TAB 500

using namespace std;

char *portName = "\\\\.\\COM11";

char incomingData[MAX_DATA_LENGTH];

//Control signals for turning on and turning off the led
//Check maple code
char avant[] = "A";
char arriere[] = "B";
char stop[] = "S";
char receive[] = "R";

long chrono[TAILLE_TAB*2];
unsigned int codeur[TAILLE_TAB*2];
unsigned int current[TAILLE_TAB*2];
unsigned int torque[TAILLE_TAB*2];
unsigned char ticks[TAILLE_TAB*2];

SerialPort *maple;

long getMicrotime(){
	struct timeval currentTime;
	gettimeofday(&currentTime, NULL);
	return currentTime.tv_sec * (int)1e6 + currentTime.tv_usec;
}

void waitmicropro(long microseconds){
	long start = getMicrotime();
	while(getMicrotime() < (start+microseconds));
}

int main()
{
	maple = new SerialPort(portName);

	//Checking if maple is connected or not
	if (maple->isConnected()) {
		std::cout << "Connection established at port " << portName << endl;
	}

	long start = getMicrotime();
	int readResult1;
	//printf("START : %lu\n",start);

	//Démarrage de la comm
	maple->writeSerialPort(avant, 1);
	waitmicropro(50);

	for (int i=0 ; i<TAILLE_TAB ; i++) {

		maple->readSerialPort(incomingData, MAX_DATA_LENGTH);
		waitmicropro(998); //à paramétrer suivant le nombre de valeurs demandées

		ticks[i] = (unsigned char)incomingData[0];
		codeur[i] = (unsigned int) (((incomingData[2] << 8) & 0xFF00) |
									(incomingData[1] & 0x00FF));
		current[i] = (unsigned int) (((incomingData[4] << 8) & 0xFF00) |
									 (incomingData[3] & 0x00FF));
		torque[i] = (unsigned int) (((incomingData[6] << 8) & 0xFF00) |
									(incomingData[5] & 0x00FF));

		chrono[i] = getMicrotime()-start;
	}

	maple->writeSerialPort(arriere, 1);
	waitmicropro(50);

	for (int i=TAILLE_TAB ; i<(TAILLE_TAB*2) ; i++) {

		maple->readSerialPort(incomingData, MAX_DATA_LENGTH);
		waitmicropro(998); //à paramétrer suivant le nombre de valeurs demandées

		ticks[i] = (unsigned char)incomingData[0];
		codeur[i] = (unsigned int) (((incomingData[2] << 8) & 0xFF00) |
									(incomingData[1] & 0x00FF));
		current[i] = (unsigned int) (((incomingData[4] << 8) & 0xFF00) |
									 (incomingData[3] & 0x00FF));
		torque[i] = (unsigned int) (((incomingData[6] << 8) & 0xFF00) |
									(incomingData[5] & 0x00FF));

		chrono[i] = getMicrotime()-start;

	}

	maple->writeSerialPort(stop, 1);
	waitmicropro(1000);

	float tempSerial;
	float rapport=((3.0f)/4095.0f);

	for (int j=0 ; j<(TAILLE_TAB*2) ; j++){
		tempSerial=((current[j]*rapport-1.5174f)*20.0f)/0.625f;
		printf(" time (PC) : %lu\t ticks : %d\tcodeur : %d\t courant : %f\t torque : %d\n",chrono[j],ticks[j], codeur[j],tempSerial, torque[j]);
	}

	delete maple;
	printf("FIN \n");
}
