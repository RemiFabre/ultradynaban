/*
 * CommonForDspic.cpp
 *
 */

#include "CommonForDspic.h"

//Simple function to help you get a proper Order code.
uint16 GetOrder(int stopState,
				int enableState,
				int torqueState,
				int pwmBridgeState,
				int qeiState,
				int absoluState,
				int currentState,
				int voutState,
				int tempBridgeState,
				int tempMotor,
				int tempBatt,
				int vBattState,
				int pwmFan1State,
				int pwmFan2State,
				int errorState,
				int configState)
{
	return (uint16) (
			(stopState << SHT_STOP) |
			(enableState << SHT_ENABLE) |
			(torqueState << SHT_Torque) |
			(pwmBridgeState << SHT_PWM_Bridge) |
			(qeiState << SHT_QEI) |
			(absoluState << SHT_Absolu) |
			(currentState << SHT_Curent_Bridge) |
			(voutState << SHT_Vout) |
			(tempBridgeState << SHT_Temp_Bridge) |
			(tempMotor << SHT_Temp_Motor) |
			(tempBatt << SHT_Temp_Bat) |
			(vBattState << SHT_V_Bat) |
			(pwmFan1State << SHT_PWM_Fan1) |
			(pwmFan2State << SHT_PWM_Fan2) |
			(errorState << SHT_Error) |
			(configState << SHT_Config));

}