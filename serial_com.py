#!/usr/bin/python
# -*- coding: utf-8 -*-

import serial
import time

def open_serial(port, baud, timeout):
    ser = serial.Serial(port=port, baudrate=baud, timeout=timeout)
    if ser.isOpen():
        return ser
    else:
        print 'SERIAL ERROR'


def close(ser):
    ser.close()


def write_data(ser, data):
    ser.write(data)


def read_data(ser, size=1):
    return ser.read(size)


def to_hex(val):
    return chr(val)


def decode_data(data):
    res = ''
    for d in data:
        res += hex(ord(d)) + ' '

    return res

def checksum(data) :
    return (~data)&0xff
    

if __name__ == '__main__':

    # we open the port
    serial_port = open_serial('/dev/ttyACM0', 57600, timeout=0.1)

    print "Go"
    write_data(serial_port, "0")
    write_data(serial_port, "\r")
    time.sleep(3)
    
    write_data(serial_port, "2")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "\r")
    time.sleep(3)

    write_data(serial_port, "5")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "\r")
    time.sleep(3)
    
    write_data(serial_port, "1")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "\r")
    time.sleep(3)
    
    write_data(serial_port, "2")
    write_data(serial_port, "5")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "\r")
    time.sleep(3)

    write_data(serial_port, "1")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "\r")
    time.sleep(3)
    
    write_data(serial_port, "2")
    write_data(serial_port, "0")
    write_data(serial_port, "0")
    write_data(serial_port, "\r")
    time.sleep(3)
    
    # read the status packet (size 6)
    #d = read_data(serial_port, 6)
    #print decode_data(d)
