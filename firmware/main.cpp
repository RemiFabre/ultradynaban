#include <wirish/wirish.h>
#include <libmaple/adc.h>
#include <libmaple/timer.h>
#include "dxl_HAL.h"
#include "magnetic_encoder.h"
#include "motor.h"
#include "control.h"
#include "dxl.h"
#include "trajectory_manager.h"
#include "flash_write.h"


#define OVER_FLOW             3000
#define BUFFER_SIZE           100
#define CLK_PIN 9
#define CS_PIN 10
#define DO1_PIN 8

/**
 * Schedules the hardware tasks
 */
void hardware_tick();

void set_ready_to_update_hardware();

int my_atoi(char *p, bool * success);

static bool     DXL_COM_ON = true;
long           counter               = 0;
unsigned int   hardwareCounter       = 0;
bool           readyToUpdateHardware = false;
char buffer[BUFFER_SIZE];
int index = 0;
HardwareTimer timer2(2);
encoder * encoder0;
unsigned char  controlMode           = OFF;
hardware       hardwareStruct;
char led_value = 0;

void setup() {
    disableDebugPorts();

    //afio_remap(AFIO_REMAP_USART1);
    //gpio_set_mode(GPIOB, 6, GPIO_AF_OUTPUT_PP);
    //gpio_set_mode(GPIOB, 7, GPIO_INPUT_FLOATING);

    // Initialization of USART
    digitalWrite(BOARD_TX_ENABLE, LOW);
    pinMode(BOARD_TX_ENABLE, OUTPUT);
    digitalWrite(BOARD_TX_ENABLE, LOW);

    digitalWrite(BOARD_RX_ENABLE, LOW);
    pinMode(BOARD_RX_ENABLE, OUTPUT);
    digitalWrite(BOARD_RX_ENABLE, HIGH);

    // Hack for priting before inits
    //Serial1.begin(57600);

    /*Setting the timer's prescale to get a 24KHz PWM.
      Pin 5 is mapped to timer3 channel1*/
    HardwareTimer timer3(3);
    timer3.setMode(1, TIMER_PWM);
    timer3.setPrescaleFactor(1);
    timer3.setOverflow(OVER_FLOW);

    pinMode(BOARD_LED_PIN, OUTPUT);

        //TEMP !
    //    SerialUSB.end();
    /*    Serial1.begin(9600);
    digitalWrite(BOARD_LED_PIN, HIGH);
    while(1) {
        digitalWrite(BOARD_TX_ENABLE, HIGH);
        Serial1.println("Please work for the sake of god");
        Serial1.waitDataToBeSent();
        digitalWrite(BOARD_TX_ENABLE, LOW);
	delay(200);
	SerialUSB.println("At least I work");
	delay(200);
	}*/

    // ADC pin init
    //pinMode(CURRENT_ADC_PIN, INPUT_ANALOG);
    //pinMode(POWER_SUPPLY_ADC_PIN, INPUT_ANALOG);
    //pinMode(TEMPERATURE_ADC_PIN, INPUT_ANALOG);

    //Encoder init and firt read of position
    encoder_init_sharing_pins_mode(CLK_PIN, CS_PIN);
    encoder_add_encoder_sharing_pins_mode(DO1_PIN);
    encoder_read_angles_sharing_pins_mode();

    //Dxl
    dxl_init();

    //Motor init
    motor_init(encoder_get_encoder(0));

    //Control
    control_init();

    //Traj
    predictive_control_init();

    hardwareStruct.enc = encoder_get_encoder(0);
    hardwareStruct.mot = motor_get_motor();
    //hardwareStruct.temperature = read_temperature();
    //hardwareStruct.voltage = read_voltage();

    timer2.pause();
    // The hardware will be read at 1Khz
    timer2.setPrescaleFactor(72);
    timer2.setOverflow(1000);
    timer2.setChannel1Mode(TIMER_OUTPUT_COMPARE);
    //Interrupt 1 count after each update
    timer2.setCompare(TIMER_CH1, 1);
    timer2.attachCompare1Interrupt(set_ready_to_update_hardware);
    timer2.refresh();
    timer2.resume();

    //Dxl struct init. Values that depend on previous inits (motor struct mainly)
    init_dxl_ram();
    init_dxl_eeprom();
}


void loop() {
    if (DXL_COM_ON) {
        if (dxl_tick()) {
            read_dxl_eeprom();
            read_dxl_ram();
            digitalWrite(BOARD_LED_PIN, led_value);
            if (led_value) {
                led_value = 0;
            } else {
                led_value = 1;
            }
        }
    }

    if (readyToUpdateHardware) {
        counter++;
        readyToUpdateHardware = false;
        hardware_tick();
        if (DXL_COM_ON) {
            update_dxl_ram();
        }
    }

    //if (readyToUpdateCurrent) {
    //readyToUpdateCurrent = false;
    //motor_read_current();
    //}

}

void hardware_tick() {
    //These actions are performed at a rate of 1kHz

    // add_benchmark_time();
        //Updating the encoder (~90-92 us)
    encoder_read_angles_sharing_pins_mode();
    // add_benchmark_time();
        //Updating the motor (~5-6 us with predictive control on)
    motor_update(hardwareStruct.enc);

    // add_benchmark_time();
        //Updating control (3-4 us)
    if (controlMode == POSITION_CONTROL) {
        predictiveCommandOn = false;
        control_tick_PID_on_position(hardwareStruct.mot);
    } else if (controlMode == SPEED_CONTROL) {
        predictiveCommandOn = false;
        control_tick_P_on_speed(hardwareStruct.mot);
    } else if (controlMode == ACCELERATION_CONTROL) {
        predictiveCommandOn = false;
        control_tick_P_on_acceleration(hardwareStruct.mot);
    } else if (controlMode == TORQUE_CONTROL) {
        predictiveCommandOn = false;
        control_tick_P_on_torque(hardwareStruct.mot);
    } else if (controlMode == POSITION_CONTROL_P) {
        predictiveCommandOn = false;
        control_tick_P_on_position(hardwareStruct.mot);
    } else if (controlMode == PREDICTIVE_COMMAND_ONLY) {
        predictiveCommandOn = true;
        control_tick_predictive_command_only(hardwareStruct.mot);
    }  else if (controlMode == PID_AND_PREDICTIVE_COMMAND) {
        predictiveCommandOn = true;
        control_tick_PID_and_predictive_command(hardwareStruct.mot);
    } else if (controlMode == PID_ONLY) {
            // PID only, but uses the trajectory system (where POSITION_CONTROL does not)
        predictiveCommandOn = true;
        control_tick_PID_on_position(hardwareStruct.mot);
    } else if (controlMode == COMPLIANT_KIND_OF) {
        predictiveCommandOn = true;
        control_tick_predictive_command_only(hardwareStruct.mot);
    } else if (controlMode == CURRENT_CONTROL) {
        //Disabled, current measure is not reliable
    } else {
        predictiveCommandOn = false;
            // No control
    }
    // add_benchmark_time();
    hardwareCounter++;
    if (hardwareCounter & (1 << 10)) {
        //These actions are performed at ~1 Hz (0.98Hz)
        hardwareCounter = 0;
        //TODO read temperature and voltage
    }
}

void set_ready_to_update_hardware() {
    readyToUpdateHardware = true;
}


// Force init to be called *first*, i.e. before static object allocation.
// Otherwise, statically allocated objects that need libmaple may fail.
__attribute__((constructor)) void premain() {
    init();
}

int main(void) {
    setup();

    while (true) {
        loop();
    }

    return 0;
}


int my_atoi(char *p, bool * success) {
    int k = 0;
    int sign = 1;
    if (*p == '-') {
        sign = -1;
        p++;
    }
    while (*p != '\0') {
        int value = *p - '0';
        if (value >=0 && value <= 9) {
            k = k*10 + value;
            p++;
        } else {
            *success = false;
            return 0;
        }
    }
    *success = true;
    return k*sign;
}
