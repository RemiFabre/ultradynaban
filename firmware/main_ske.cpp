#include <stdlib.h>
#include <wirish/wirish.h>
#include <servos.h>
#include <terminal.h>
#include <main.h>

TERMINAL_PARAMETER_INT(t, "Variable t", 0);

TERMINAL_COMMAND(hello, "Prints hello world")
{
    terminal_io()->println("Hello world");
}

/**
 * Setup function
 */
void setup()
{
  Serial1.begin(9600);
}

/**
 * Loop function
 */
void loop()
{
  Serial1.print("Yo");
}
